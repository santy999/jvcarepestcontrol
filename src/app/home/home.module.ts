import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { LandingPageComponent,QsrServiceDialog,ResidenceServiceDialog,CommercialServiceDialog,
     PestProofingServiceDialog,CCCServiceDialog,BedBugControlDialog,AntEliminationDialog,
     MosquitoEliminationDialog,loginpop } from './landing-page/landing-page.component';
import { LayoutModule } from '../layout/layout.module';
import { DemoMaterialModule } from '../layout/material-module';
import {Ng2PageScrollModule} from 'ng2-page-scroll';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [LandingPageComponent,QsrServiceDialog,ResidenceServiceDialog,CommercialServiceDialog,
     PestProofingServiceDialog,CCCServiceDialog,BedBugControlDialog,AntEliminationDialog,
     MosquitoEliminationDialog,loginpop],
  imports: [
    CommonModule,
    HomeRoutingModule,
    LayoutModule,
    DemoMaterialModule,
    Ng2PageScrollModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule
  ],
   entryComponents: [
   loginpop
    
  ],
})
export class HomeModule { }
