import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent,QsrServiceDialog,ResidenceServiceDialog,CommercialServiceDialog,
		 PestProofingServiceDialog,CCCServiceDialog,BedBugControlDialog,AntEliminationDialog,
		 MosquitoEliminationDialog} from './landing-page/landing-page.component';

const routes: Routes = [
	{ path: '', component: LandingPageComponent},
	{ path : 'qsrService', component : QsrServiceDialog},
	{ path : 'residenceService', component: ResidenceServiceDialog},
	{ path : 'commercialser', component : CommercialServiceDialog},
	{ path : 'pestProofing', component : PestProofingServiceDialog},
	{ path : 'cccserv', component : CCCServiceDialog},
	{ path : 'bedBugContrl', component : BedBugControlDialog},
	{ path : 'antElimination', component : AntEliminationDialog},
	{ path : 'mosqElimination', component : MosquitoEliminationDialog}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
