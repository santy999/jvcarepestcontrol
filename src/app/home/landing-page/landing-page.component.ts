import { Component, OnInit,Inject, ElementRef, ViewChild } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import { Router ,ActivatedRoute } from '@angular/router';
import { ApplicationUrls } from '../../layout/ApplicationUrls';
import { RestService } from '../../layout/Services/RestService/rest.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import { DemoMaterialModule } from '../../layout/material-module';
declare var $:any;

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  @ViewChild('appResponse',{static: false}) openAppModal:ElementRef;

  constructor( public dialog:MatDialog,
               public router : Router,
               private restServ: RestService,
               private _formBuilder : FormBuilder) { }

  public enquiryForm : FormGroup;
  public resFromApp;
  public Loading = false;

  ngOnInit() {

    this.enquiryForm = this._formBuilder.group({

      firstName : ['',[Validators.required,Validators.minLength(2),Validators.maxLength(15)]],
      emailId : ['',[Validators.required,Validators.email]],
      mobileNumber : ['',[Validators.required,Validators.minLength(10),
                      Validators.maxLength(10),
                      Validators.pattern(/^[6-9]\d{9}$/)]],
      areaPincode : ['',[Validators.required,Validators.minLength(6),
                         Validators.maxLength(6)]]
  
      });
  }


openLogin(){

  const dialogRef = this.dialog.open(loginpop,{
      width : '36%'

    
  });

}

finalDataSubmit(){
  let userData= {"name":"ningu","age":"40","gender":"f"}
  this.restServ.dataPost(userData, ApplicationUrls.saveUserData).subscribe(resp =>{
  });
}

openNavIs = true;
closeNavIs = false;
openNavTab(){
  this.openNavIs = false;
  this.closeNavIs = true;

}

closeNavTab(){
  this.openNavIs = true;
  this.closeNavIs = false;
}

goToHome(){
      $(this.openAppModal.nativeElement).modal('hide');
      this.openNavIs = true;
      this.closeNavIs = false;
}

submitForm(){
    this.Loading = true;
  let data = this.enquiryForm.value;
  console.log("Form Data",data,this.enquiryForm);
  this.restServ.dataPost(data,ApplicationUrls.applicationForm).subscribe(response =>{
    console.log(response);
    this.resFromApp = response === "Success" ? 'Your Application Submited Successfully' :
                      'Application Submition Failed';
    this.Loading = false;
    $(this.openAppModal.nativeElement).modal('show');
    this.enquiryForm.reset();
  });
}




}


@Component({
  selector: 'qsr-service-dialog',
  templateUrl: 'qsrservice-dialog.html',
  styleUrls: ['./landing-page.component.css']
})
export class QsrServiceDialog {

  constructor(public router:Router) {}

  goToHome(){
   this.router.navigate(['./home']);

 }

}

@Component({
  selector: 'residence-service-dialog',
  templateUrl: 'residenceservice-dialog.html',
  styleUrls: ['./landing-page.component.css']
})
export class ResidenceServiceDialog {

  constructor(public router : Router) {}

  goToHome(){
   this.router.navigate(['./home']);

 }

}


@Component({
  selector: 'commercial-service-dialog',
  templateUrl: 'commericialservice-dialog.html',
  styleUrls: ['./landing-page.component.css']
})
export class CommercialServiceDialog {

  constructor(public router:Router) {}

 goToHome(){
   this.router.navigate(['./home']);

 }

}

@Component({
  selector: 'pestproofing-service-dialog',
  templateUrl: 'pestproofingservice-dialog.html',
  styleUrls: ['./landing-page.component.css']
})
export class PestProofingServiceDialog {

  constructor(public router:Router ) {}

 goToHome(){
   this.router.navigate(['./home']);

 }
}


@Component({
  selector: 'cce-service-dialog',
  templateUrl: 'cce-dialog.html',
  styleUrls: ['./landing-page.component.css']
})
export class CCCServiceDialog {

  constructor(public router : Router ) {}

 goToHome(){
   this.router.navigate(['./home']);

 }

}



@Component({
  selector: 'BedBugControl-dialog',
  templateUrl: 'bedbugcontrol-dialog.html',
  styleUrls: ['./landing-page.component.css']
})
export class BedBugControlDialog {

  constructor(public router:Router) {}

  goToHome(){
   this.router.navigate(['./home']);

 }
 
}

@Component({
  selector: 'AntElimination-dialog',
  templateUrl: 'antelimination-dialog.html',
  styleUrls: ['./landing-page.component.css']
})
export class AntEliminationDialog {

  constructor( public router:Router ) {}

  goToHome(){
   this.router.navigate(['./home']);

 }

}

@Component({
  selector: 'MosquitoElimination-dialog',
  templateUrl: 'mosquitoelimination-dialog.html',
  styleUrls: ['./landing-page.component.css']
})
export class MosquitoEliminationDialog {

  constructor( public router:Router) {}
  goToHome(){
   this.router.navigate(['./home']);

 }

}


@Component({
  selector: 'loginsignup-dialog',
  templateUrl: 'loginpop.html',
  styleUrls: ['./landing-page.component.css']
})
export class loginpop implements OnInit {

  public accountOpening : FormGroup;
  public myOtp;
  public verOtp;
  public otpVerified = true;
  public hide;


  constructor(public dialogRef: MatDialogRef<loginpop>,
              public _formBuilder:FormBuilder,
              private restServ: RestService,
              private _matSnack:MatSnackBar)  {}

  ngOnInit(){
    this.accountOpening = this._formBuilder.group({
      userName : [''],
      otp : [''],
      password : ['']
    });
    this.accountOpening.get('userName').valueChanges.subscribe(value =>{
      if (value.length == 10) {
        this.restServ.dataGet(ApplicationUrls.getOtp).subscribe(otp =>{
          console.log("My Otp",otp)
          this.myOtp = otp
        });
      }
    });


    this.accountOpening.get('otp').valueChanges.subscribe(verifie =>{
      this.verOtp = verifie;
      console.log("verifieeee",this.verOtp,this.myOtp);
      if (this.verOtp.length == 6) {
        if(this.myOtp==this.verOtp){
        console.log("OTP Verified")
        this.otpVerified = false;
        let snackBarRef = this._matSnack.open('Otp Successfully Verified!!','OK',{
          duration : 3000
        });
        } else{
        console.log("Otp is wrong");
        }
      }
     
    });
  }

hideLogin = true;
createAcc = false;

  createAccount(){
    this.hideLogin = false;
    this.createAcc = true;
      }

  onNoClick(): void {
    this.dialogRef.close();
  }

public messageForAcc;
  accCreationSubmission(){
    console.log(this.accountOpening.value);
    let data = this.accountOpening.value;
    this.restServ.dataPost(data,ApplicationUrls.createMyAccount).subscribe(res =>{
      console.log("ACC Successfully Created",res);
      this.messageForAcc = res;
    });
  }
}




