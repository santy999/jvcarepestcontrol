import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor( private http: HttpClient) { }
  private BASE_URL = "http://127.0.0.1:8000/";

  public dataPost(data,url): Observable <any>{
  	return this.http.post(this.fullUrl(url),data);
  }
  public dataGet(url): Observable <any>{
  	return this.http.get(this.fullUrl(url));
  }

  public fullUrl(url){
  	return this.BASE_URL+url;
  }
}
