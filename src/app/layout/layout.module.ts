import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { CoreService } from './Services/CoreService/core.service';
import { RestService } from './Services/RestService/rest.service';
import { DemoMaterialModule } from './material-module';

@NgModule({
  declarations: [FooterComponent, HeaderComponent],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    DemoMaterialModule
  ],
  providers : [CoreService,RestService],
})
export class LayoutModule { }
 